<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 21:42
 */
require_once 'SelectorIndividual.php';
class SISelect extends SelectorIndividual
{

    public function Selector(): string
    {
        $i=0;

        $selector="<label>$this->titulo</label>";
        $selector.="<select name='this->nombre'>";
        foreach ($this->elementos as $clave=>$valor){
            if ($this->seleccionado ===$i)
                $selecionado='selected';
            else
                $selecionado='';
            $selector .= sprintf(
                "<option value='%s' %s>%s</option>",
                $clave,
                $selecionado,
                $valor
            );
            $i++;
        }
        $selector .="</select>";
        return $selector;
    }

}