<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 20:29
 */

abstract class SelectorIndividual implements ISelectorIndividual
{
    protected $titulo;
    protected $nombre;
    protected $elementos;
    protected $seleccionado;
    public function __construct($titulo, $nombre, array $elementos, $seleccionado = 0)
    {
        $this->titulo=$titulo;
        $this->nombre=$nombre;
        $this->elementos=$elementos;
        $seleccionado->seleccionado=$seleccionado;

    }

    public abstract function Selector(): string;



}