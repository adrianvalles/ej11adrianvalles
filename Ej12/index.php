<?php
require_once 'SIRadioOpcion.php';
require_once 'SISelect.php';

$selectCiudad= new SISelect('ciudad','ciudad',['Alicante','Valencia','Castellón']);
$selectNivelIdioma= new SISelect('Nivel idioma','nivelIdioma',['Alto','Medio','Bajo']);
$radioSexo= new SIRadioOpcion('Sexo','sexo',['Hombre','Mujer']);
$radioEstado = new SIRadioOpcion('Estado','estado',['Encendido','Apagado']);

require 'views/index.view.php';