<?php
    require 'utils/utils.php';

    if($_SERVER['REQUEST_METHOD'] === 'POST')
			{
				$nombre = trim(htmlspecialchars($_POST['nombre']));
				$apellido = trim(htmlspecialchars($_POST['apellido']));
				$fecha = trim(htmlspecialchars($_POST['fecha']));
				$email = trim(htmlspecialchars($_POST['email']));

				if(!empty($nombre) && !empty($fecha) && !empty($email))
					{
						echo "<p>Nombre: $nombre</p>";

						echo "<p>Apellido: $apellido</p>";

						if(DateTime::createFromFormat('d/m/y', $fecha) === false)
							{
								echo "<p>La fecha no es correcta no es correcta. (D/M/A)</p>";
							}
						else
							{
								echo "<p>Fecha: $fecha</p>";
							}
						
						if(filter_var($email, FILTER_VALIDATE_EMAIL) === false)
							{
								echo "<p>El correo electronico no es correcto</p>";
							}
						else
							{
								echo "<p>Email: $email</p>";
							}
					}
				else
					{
						echo "Alguno de los campos reqeridos está vacio.";
					}
			} 

    require 'views/contact.view.php';