<?php

class ImageGallery
{
    const RUTA_IMAGENES_PORTFOLIO='images/index/portfolio/';
    const RUTA_IMAGENES_GALLERY='images/index/gallery/';
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $descripcion;
    /**
     * @var int
     */
    private $numVisualizaciones;
    /**
     * @var int
     */
    private $numLikes;
    /**
     * @var int
     */
    private $numDownloads;

    /**
     * ImageGallery constructor.
     * @param string $nombre
     * @param string $descripcion
     * @param int $numVisualizaciones
     * @param int $numLikes
     * @param int $numDownloads
     */
    public function __construct(string $nombre, string $descripcion, int $numVisualizaciones=0, int $numLikes=0, int $numDownloads=0)
    {
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDescripcion();
    }


    /**
     * @return string
     */
    public function getNombre() : string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return ImageGallery
     */
    public function setNombre(string $nombre) : ImageGallery
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion() : string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return ImageGallery
     */
    public function setDescripcion(string $descripcion) : ImageGallery
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumVisualizaciones() : int
    {
        return $this->numVisualizaciones;
    }

    /**
     * @param int $numVisualizaciones
     * @return ImageGallery
     */
    public function setNumVisualizaciones(int $numVisualizaciones) : ImageGallery
    {
        $this->numVisualizaciones = $numVisualizaciones;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumLikes() : int
    {
        return $this->numLikes;
    }

    /**
     * @param int $numLikes
     * @return ImageGallery
     */
    public function setNumLikes(int $numLikes) : ImageGallery
    {
        $this->numLikes = $numLikes;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumDownloads() : int
    {
        return $this->numDownloads;
    }

    /**
     * @param int $numDownloads
     * @return ImageGallery
     */
    public function setNumDownloads(int $numDownloads) : ImageGallery
    {
        $this->numDownloads = $numDownloads;
        return $this;
    }

    public function getUrlPortfolio() : string
    {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    public function getUrlGallery() : string
    {
        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }
    
}


?>