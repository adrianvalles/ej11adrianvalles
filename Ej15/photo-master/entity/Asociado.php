<?php

class Asociado
{

    const RUTA_IMAGEN_ASOCIADOS ='images/index/asociados/';


    private $nombre;
    private $logo;
    private $descripcion;
    

   

    public function __construct($nombre, $logo, $descripcion)
    {
       $this->nombre=$nombre;
       $this->logo=$logo;
       $this->descripcion=$descripcion;
       
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     * @return Asociado
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     * @return Asociado
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     * @return Asociado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }


    public function getUrlAsociados(){
        return self::RUTA_IMAGEN_ASOCIADOS .$this->getLogo();
    }
}


?>