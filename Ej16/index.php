<?php
require_once 'entity/ImageGallery.php';
require_once 'entity/Asociado.php';
require_once 'utils/utils.php';
$arrayImageGallery=[12];

for ($i=0;$i<12;$i++){
    $arrayImageGallery[$i]=new ImageGallery(''.($i+1).'.jpg','descripcion imagen '.($i+1));
}

$asociados=[
        new Asociado(
            'Primer asociado',
            'log1.jpg',
            'Descripción primer asociado'),
        new Asociado(
            'Segun asociado',
            'log2.jpg',
            'Descripción segundo asociado'),
        new Asociado(
            'Tercer asociado',
            'log3.jpg',
            'Descripción tercer asociado'),
];


$asociados=extraerAsociados($asociados,3);

require 'views/index.view.php';


?>