<?php
function recibeReferencia($pagina)
{

    if(strpos($_SERVER['REQUEST_URI'], $pagina)){
        
        return true;
    }else{
        
        return false;
    }
        
    
    
}

function existeOpcionMenuActivaEnArray(array $opcionesMenu):bool
{
    foreach($opcionesMenu as $opcionMenu){
        if(recibeReferencia($opcionMenu)===true){
            return true;
        }
    }
    return false;
}
function extraerAsociados(array &$asociados, int $numeroAReducir ){//LLAMADO EN INDEX.PHP

    shuffle($asociados);//MEZCLA LOS ELEMENTOS DEL ARRAY DE FORMA ALEATORIA
    $trozos=array_chunk($asociados,$numeroAReducir);
    return $trozos[0];//DEVOLVEMOS UN ARRAY DE 3 ELEMENTOS
}
?>