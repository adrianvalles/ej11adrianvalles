<?php

class Asociado{

    private $nombre;
    private $logo;
    private $descripcion;


    const RUTA_IMAGENES_ASOCIADO = 'images/index/asociados/';

    /**
     * Asociado constructor.
     * @param $nombre
     * @param $logo
     * @param $descripcion
     */
    public function __construct(string $nombre, string $logo, string $descripcion)
    {
        $this->nombre = $nombre;
        $this->logo = $logo;
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }


    public function getUrlAsociados()
    {
        return self::RUTA_IMAGENES_ASOCIADO.$this->getLogo();

    }


}


?>