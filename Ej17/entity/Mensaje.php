<?php
require_once __DIR__.'/../database/iEntity.php';

class Mensaje implements IEntity
{
    private $id;
    private $nombre;
    private $apellidos;
    private $asunto;
    private $email;
    private $texto;
    private $fecha;

    /**
     * Mensaje constructor.
     * @param $id
     * @param $nombre
     * @param $apellidos
     * @param $asunto
     * @param $email
     * @param $texto
     * @param $fecha
     */
    public function __construct(string $nombre, string $apellidos, string $asunto, string $email, string $texto, string $fecha)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->asunto = $asunto;
        $this->email = $email;
        $this->texto = $texto;
        $this->fecha = $fecha;
    }


    public function toArray(): array
    {
        // TODO: Implement toArray() method.
    }
}