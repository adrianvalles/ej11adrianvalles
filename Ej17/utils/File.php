<?php
require_once  __DIR__ . '/../exceptions/FileException.php';

class File{

    private $file;
    private $filename;

    public function __construct(string $filename, arr $arrTipos)
    {
        $this->file = $_FILES[$filename];
        $this->filename = '';

        if(!isset($file)){
            throw new FileException('Debes seleccionar un archivo.');
        }

        if($this->file['error'] !== UPLOAD_ERR_OK){
            switch ($this->file['error']){
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                //Error al sobrepasar el tamaño de archivo permitido en el formulario
                throw new FileException('El tamaño del archivo es demasiado grande.');
                case UPLOAD_ERR_PARTIAL:
                //Error al subir un archivo incompleto por corte de conexion
                    throw new FileException('El archivo se ha subido de forma incompleta.');
                default:
                    throw new FileException('Ha habido un error al subir el archivo.');
                    break;
            }

        }

        if(in_array($this->file['type'], $arrTipos) === false)
            throw new FileException('El tipo de archivo que has subido no esta permitido.');
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    public function saveUploadFile(string $destino){
        if(is_uploaded_file($this->file['tmp_name']) === false)
            throw new FileException('El archivo no se ha subido a traves del formulario');

        $this->filename = $this->file['name'];

        $ruta = $destino.$this->filename;

        if(is_file($ruta) === true){
            $idU = time();
            $this->filename = $idU.'_'.$this->filename;
            $ruta = $destino.$this->filename;
        }

        if(move_uploaded_file($this->file['tmp_name'], $ruta) === false)
            throw new FileException('No se puede mover el archivo a su destino.');

    }

    public function copyFile(string $origen, string $destino){

        $o = $origen.$this->filename;
        $d = $destino.$this->filename;

        if(is_file($origen) === false)
            throw new FileException("No existe el archivo $origen que estas intentando copiar.");

        if(is_file($destino) === true)
            throw new FileException("El archivo $destino ya existe y no se puede sobreescribir");

        if(copy($origen,$destino) === false)
            throw new FileException("No se ha podido copiar el archivo $origen a $destino");

    }



}


?>