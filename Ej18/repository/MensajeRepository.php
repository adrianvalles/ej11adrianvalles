<?php
require_once __DIR__.'/../database/QueryBuilder.php';

class MensajeRepository extends QueryBuilder
{

    public function __construct(string $table='Mensajes',string $classEntity='Mensaje')
    {
        parent::__construct($table,$classEntity);
    }
}