<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 11:50
 */
require_once 'IElementoHTML.php';
abstract class ElementoHTML implements IElementoHTML
{
    protected $nombre;
    protected $atributo;


    public function __construct( $nombre)
    {
        $this->nombre = $nombre;
        $this->atributos = [];
    }




    public function __set($nombre, $value)
    {
        $this->atributos[$nombre] = $value;
    }

    public function __get($name)
    {
       return $this->atributos[$name];
    }

    abstract public function __toString();


}