<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 12:41
 */
require_once 'ElementoHTMLSimple.php';
abstract class ElementoHTMLCompuesto extends ElementoHTMLSimple
{
private $hijos;
public function __construct($nombre)
{
    parent::__construct($nombre);
    $this->hijos=[];
}
public function nuevoHijo(IElementoHTML $hijo){
    $this->hijos[]=$hijo;

}
public function __toString()
{
    $etiqueta= parent::__toString();
    foreach ($this->hijos as $hijo)
        $etiqueta.=$hijo;
    $etiqueta .='</' .$this->nombre .'>';
    return$etiqueta;

}
}