<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 12:22
 */
require_once 'ElementoHTMLSimple.php';
abstract class ElementoHTMLConTexto extends  ElementoHTMLSimple

{
private $texto;
public function  __construct($nombre, $texto)
{
    parent::_construct($nombre);
    $this->texto=$texto;


}
public function __toString()
{
    $etiqueta = parent::__toString();
    $etiqueta .= $this->texto;
    $etiqueta .= '</' . $this->nombre .'>';

    return $etiqueta;
}
}