<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 12:00
 */
require_once 'ElementoHTML.php';
abstract class ElementoHTMLSimple extends ElementoHTML
{

    public function __toString()
    {
        $etiqueta= '<'.$this->nombre;
        foreach ($this->atributos as $key =>$value){
            $etiqueta .=''. $key . '='.$value .'';

            $etiqueta .='>';

            return $etiqueta;
        }
    }
}
