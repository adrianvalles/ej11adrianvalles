<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 12:29
 */
require_once 'ElementoHTMLConTexto.php';
class ElementoParrafo extends  ElementoHTMLConTexto
{
public  function __construct( $texto)
{
    parent::__construct('p', $texto);
}
}