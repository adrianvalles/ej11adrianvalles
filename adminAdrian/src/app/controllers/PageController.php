<?php
namespace ProyectoWeb\app\controllers;

use Psr\Container\ContainerInterface;


class PageController
{
    protected $container;

    // constructor receives container instance
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    public function home($request, $response, $args) {
        $pageheader = "Categorias";
        return $response->write($pageheader);
        //return $this->container->renderer->render($response, "index.view.php", compact('title'));
        
   }

}
